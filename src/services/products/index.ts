import axios, { AxiosInstance, AxiosResponse } from 'axios';
import { IProduct } from 'src/models/product';
import EnvironmentService from 'src/services/environment';
import { Container, Singleton } from 'typescript-ioc';

export interface IProductGetResponse {
  data: IProduct;
}

export interface IProductGetAllResponse {
  data: IProduct[];
}

export interface IProductCreateRequest {
  data: Omit<IProduct, 'id'>;
}

export interface IProductCreateResponse {
  data: IProduct;
}

export interface IProductUpdateRequest {
  data: Partial<IProduct>;
}

export interface IProductUpdateResponse {
  data: IProduct;
}

export interface IProductDeleteRequest {
  data: Pick<IProduct, 'id'>;
}

export interface IProductCheckSkuRequest {
  data: Pick<IProduct, 'sku'>;
}

export interface IProductCheckSkuResponse {
  data: {
    inUse: boolean;
  }
}

@Singleton
export default class ProductsService {
  public readonly ROOT_PATH = 'products';
  private clientInstance?: AxiosInstance;
  private environmentService = Container.get(EnvironmentService);

  private get client() {
    if (!this.clientInstance) {
      this.clientInstance = axios.create({
        baseURL: `${this.environmentService.apiHost}:${this.environmentService.apiPort}/${this.ROOT_PATH}`
      })
    }

    return this.clientInstance;
  }

  public async checkSku(data: IProductCheckSkuRequest) {
    return await this.client.post<IProductCheckSkuRequest, AxiosResponse<IProductCheckSkuResponse>>(`/sku`, data);
  }

  public async create(data: IProductCreateRequest) {
    return await this.client.post<IProductCreateRequest, AxiosResponse<IProductCreateResponse>>('', data);
  }

  public async delete(data: IProductDeleteRequest) {
    return await this.client.delete<IProductDeleteRequest, AxiosResponse<void>>('', { data });
  }

  public async get(id: string | number) {
    return await this.client.get<void, AxiosResponse<IProductGetResponse>>(`/${id}`);
  }

  public async getAll() {
    return await this.client.get<void, AxiosResponse<IProductGetAllResponse>>('');
  }

  public async update(id: number | string, data: IProductUpdateRequest) {
    return await this.client.put<IProductUpdateRequest, AxiosResponse<IProductUpdateResponse>>(`/${id}`, data);
  }
}
