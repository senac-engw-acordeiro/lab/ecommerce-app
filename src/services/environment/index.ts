import { Env, loadConfig } from 'env-decorator';
import { Factory, Singleton } from "typescript-ioc";

@Singleton
@Factory(() => {
  const config = loadConfig(EnvironmentService);
  
  const service = new EnvironmentService();
  
  return Object.assign(service, config);
})
export default class EnvironmentService {
  @Env('REACT_APP_API_HOST') public apiHost!: string;
  @Env('REACT_APP_API_PORT', { type: 'number' }) public apiPort!: number;
}