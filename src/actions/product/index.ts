import { createActionStages, IReducerActionCollection } from 'src/actions';
import { IProduct } from 'src/models/product';

export interface IProductCreateRequest extends IProduct {}

export interface IProductCreateSucceeded extends IProduct {}

export interface IProductCreateFailed {
  message: string;
}

export interface IProductEditRequest extends IProduct {}

export interface IProductEditSucceeded {}

export interface IProductEditFailed {
  message: string;
}

export interface IProductRemoveRequest extends Pick<IProduct, 'id'> {}

export interface IProductRemoveSucceeded {}

export interface IProductRemoveFailed {
  message: string;
}

export interface IProductListSucceeded {
  data: IProduct[]
}

export interface IProductListFailed {
  message: string;
}

const NAMESPACE = 'PRODUCT';

export const CREATE_PRODUCT = createActionStages(NAMESPACE + '_CREATE');

export const createProduct: IReducerActionCollection<IProductCreateRequest, IProductCreateSucceeded, IProductCreateFailed, any> = {
  request: (payload) => ({ type: CREATE_PRODUCT.REQUEST, payload }),
  succeeded: (payload) => ({ type: CREATE_PRODUCT.SUCCEEDED, payload }),
  failed: (payload) => ({ type: CREATE_PRODUCT.FAILED, payload }),
}

export const EDIT_PRODUCT = createActionStages(NAMESPACE + '_EDIT');

export const editProduct: IReducerActionCollection<IProductEditRequest, IProductEditSucceeded, IProductEditFailed, any> = {
  request: (payload) => ({ type: EDIT_PRODUCT.REQUEST, payload }),
  succeeded: (payload) => ({ type: EDIT_PRODUCT.SUCCEEDED, payload }),
  failed: (payload) => ({ type: EDIT_PRODUCT.FAILED, payload }),
}

export const REMOVE_PRODUCT = createActionStages(NAMESPACE + '_REMOVE');

export const removeProduct: IReducerActionCollection<IProductRemoveRequest, IProductRemoveSucceeded, IProductRemoveFailed, any> = {
  request: (payload) => ({ type: REMOVE_PRODUCT.REQUEST, payload }),
  succeeded: (payload) => ({ type: REMOVE_PRODUCT.SUCCEEDED, payload }),
  failed: (payload) => ({ type: REMOVE_PRODUCT.FAILED, payload }),
}

export const LIST_PRODUCT = createActionStages(NAMESPACE + '_LIST');

export const listProduct: IReducerActionCollection<void, IProductListSucceeded, IProductListFailed, any> = {
  request: (payload) => ({ type: LIST_PRODUCT.REQUEST, payload }),
  succeeded: (payload) => ({ type: LIST_PRODUCT.SUCCEEDED, payload }),
  failed: (payload) => ({ type: LIST_PRODUCT.FAILED, payload }),
}