export enum Stage {
  Request = 'REQUEST',
  Succeeded = 'SUCCEEDED',
  Failed = 'FAILED',
  Cancelled = 'CANCELLED'
}

export interface IReducerActionCollection<R, S, F, C> {
  request: ReducerAction<R, Stage.Request>;
  succeeded: ReducerAction<S, Stage.Succeeded>;
  failed: ReducerAction<F, Stage.Failed>;
  cancelled?: ReducerAction<C, Stage.Cancelled>;
}

export type ReducerAction<T, Stage> = (payload: T, stage?: Stage) => IReducerObjectAction<T>;

export interface IReducerObjectAction<T> {
  type: string;
  payload: T;
}

export function createActionStages(base: string): { [key in Stage]: string } {
  const actions = {};

  Object.values(Stage).forEach((stage) => {
    Object.defineProperty(actions, stage, {
      value: `${base}_${stage}`
    })
  }, {})

  return actions as { [key in Stage]: string };
}