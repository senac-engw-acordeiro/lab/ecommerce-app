import { Button, CircularProgress, createStyles, InputAdornment, makeStyles, Theme } from '@material-ui/core';
import { FormApi, SubmissionErrors } from 'final-form';
import { makeValidate, TextField } from 'mui-rff';
import numbro from 'numbro';
import React from 'react';
import { Form } from 'react-final-form';
import MakeAsyncFunction from 'react-redux-promise-listener';
import { useHistory } from 'react-router-dom';
import { Stage } from 'src/actions';
import { createProduct, editProduct } from 'src/actions/product';
import { IProduct, Product } from 'src/models/product';
import { promiseListener } from 'src/redux';
import { ROUTES } from 'src/routes';
import ProductsService from 'src/services/products';
import { Container } from 'typescript-ioc';
import { number, object, string } from 'yup';

const productsService = Container.get(ProductsService);

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    margin: {
      margin: theme.spacing(1),
    },
    extendedIcon: {
      marginRight: theme.spacing(1),
    },
  }),
);

const productSchema = object<Partial<IProduct>>({
  sku: string()
    .required()
    .trim()
    .uppercase(),
  name: string()
    .required(),
  description: string()
    .required(),
  price: number()
    .required()
    .positive(),
  quantity: number()
    .required()
    .positive(),
  imageSrc: string()
    .notRequired()
    .url()
    .nullable()
}).defined();

const parseNumber = (value: any) =>
  isNaN(value)
    ? 0
    : numbro.unformat(value)

export default ({ edit, data, action }: { edit?: boolean, data?: IProduct, action: { [key in Stage]: string } }) => {
  const history = useHistory();
  const classes = useStyles();

  return (
    <MakeAsyncFunction
      listener={promiseListener}
      start={action.REQUEST}
      resolve={action.SUCCEEDED}
      reject={action.FAILED}
      setPayload={(action: any, payload: IProduct) => edit ? editProduct.request(new Product(payload)) : createProduct.request(new Product(payload))}
    >{(asyncFunc: (payload: any) => Promise<any>) => {
      const handleSubmit = async (values: IProduct, form: FormApi<IProduct, Partial<IProduct>>, callback?: ((errors?: SubmissionErrors | undefined) => void)) => {
        if (!edit) {
          const response = await productsService.checkSku({ data: { sku: values.sku } });

          if (response?.data?.data?.inUse) {
            return { sku: 'SKU em uso' };
          }
        }

        const response: Promise<IProduct | { message: string }> = await asyncFunc(values);

        if ('id' in response) {
          return history.push(ROUTES.default.path);
        }
      }

      return (
        <Form
          initialValues={data}
          onSubmit={handleSubmit}
          validate={makeValidate(productSchema)}
          render={({ handleSubmit, invalid, submitting, pristine }) => (
            <form onSubmit={handleSubmit}>
              <div>
                <TextField
                  disabled={edit}
                  name='sku'
                  label='SKU'
                  className={classes.margin}
                />
                <TextField
                  name='name'
                  label='Nome'
                  className={classes.margin}
                />
                <TextField
                  name='description'
                  label='Descrição'
                  className={classes.margin}
                />
                <TextField
                  name='price'
                  label='Preço'
                  fieldProps={{ parse: parseNumber }}
                  className={classes.margin}
                  InputProps={{
                    startAdornment: <InputAdornment position='start'>R$</InputAdornment>
                  }}
                />
                <TextField
                  name='quantity'
                  type='number'
                  label='Quantidade'
                  fieldProps={{ parse: parseNumber }}
                  className={classes.margin}
                />
                <TextField
                  name='imageSrc'
                  label='Imagem'
                  className={classes.margin}
                />
              </div>
              <Button
                className={classes.margin}
                disabled={submitting || pristine}
                type='submit'
                variant='contained'
                color='primary'
                size='large'
                fullWidth
              > 
                {submitting && <CircularProgress size={24} color='primary' style={{ marginRight: 8 }} />}
                {edit ? 'Editar' : 'Cadastrar'}
              </Button>
            </form>
          )}
        />
      )}}
    </MakeAsyncFunction>
  )
}