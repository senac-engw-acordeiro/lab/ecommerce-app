import Button from '@material-ui/core/Button'
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import IconButton from '@material-ui/core/IconButton';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';
import AddCircle from '@material-ui/icons/AddCircle'
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import React from 'react';
import { useSelector } from 'react-redux';
import { IProduct } from 'src/models/product';

export type ChangeCallback = (id: number) => any;

const currencyFormatter = new Intl.NumberFormat('pt-BR', { style: 'currency', currency: 'BRL' });

const renderProductList = (
  data: IProduct[],
  onEdit: ChangeCallback = () => {},
  onRemove: ChangeCallback = () => {}
) => data.map(product =>
  <TableRow key={product.id}>
    <TableCell component='th' scope='row'>{product.sku}</TableCell>
    <TableCell align='right'>{product.name}</TableCell>
    <TableCell align='right'>{currencyFormatter.format(product.price)}</TableCell>
    <TableCell align='right'>{product.quantity}</TableCell>
    <TableCell align='right'>{product.description}</TableCell>
    <TableCell padding='checkbox'>
      <IconButton onClick={() => onEdit(product.id)}>
        <EditIcon />
      </IconButton>
    </TableCell>
    <TableCell padding='checkbox'>
      <IconButton onClick={() => onRemove(product.id)}>
        <DeleteIcon />
      </IconButton>
    </TableCell>
  </TableRow>
)

export default ({ onEdit, onRemove, onAdd }: { onEdit?: ChangeCallback, onRemove?: ChangeCallback, onAdd?: () => any }) => {
  const productList: IProduct[] = useSelector((state: any) => state.product?.list);

  return Array.isArray(productList) && productList.length > 0
  ? (
    <TableContainer component={Paper}>
      <Table>
        <TableHead>
          <TableRow>
            <TableCell>SKU</TableCell>
            <TableCell align='right'>Nome</TableCell>
            <TableCell align='right'>Preço</TableCell>
            <TableCell align='right'>Quantidade</TableCell>
            <TableCell align='right'>Descrição</TableCell>
            <TableCell />
            <TableCell />
          </TableRow>
        </TableHead>
        <TableBody>
          {renderProductList(productList, onEdit, onRemove)}
        </TableBody>
      </Table>
    </TableContainer>
  )
  : (
    <Container >
      <Grid container justify='center'>
        <Button color='primary' fullWidth style={{ maxWidth: 600, textTransform: 'none', height: 90 }} onClick={onAdd}>
          <AddCircle style={{ marginRight: 5 }} />
          <Typography color='inherit' variant='h6'>
            Registrar Produto
          </Typography>
        </Button>
      </Grid>
    </Container>
  )
}