import { CssBaseline } from '@material-ui/core';
import React from 'react';

import MainRoute from './routes';

import 'typeface-roboto';

export default class App extends React.PureComponent {
  public render() {
    return (
      <React.Fragment>
        <CssBaseline />
        <MainRoute />
      </React.Fragment>
    );
  }
}
