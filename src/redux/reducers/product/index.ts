import { persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import { CREATE_PRODUCT, EDIT_PRODUCT, LIST_PRODUCT, REMOVE_PRODUCT } from 'src/actions/product';
import { IProduct } from 'src/models/product';

const reducer = (state: { list: IProduct[] } = { list: [] }, action: { type: string, payload: any }) => {
  switch (action.type) {
    case CREATE_PRODUCT.SUCCEEDED: {
      return {
        ...state,
        list: state.list?.concat(action?.payload)
      }
    }

    case EDIT_PRODUCT.SUCCEEDED: {
      return {
        ...state,
        list: state.list?.map((item) => item.id !== action?.payload?.id ? item : { ...item, ...action.payload })
      }
    }

    case REMOVE_PRODUCT.SUCCEEDED: {
      return {
        ...state,
        list: state.list?.filter((item) => item.id !== action?.payload?.id)
      }
    }

    case LIST_PRODUCT.SUCCEEDED: {
      return {
        ...state,
        list: action?.payload?.data
      }
    }

    default: {
      return state;
    }
  }
}

const persistConfig = {
  key: 'product',
  storage
}

const persistedReducer = persistReducer(persistConfig, reducer);

export default persistedReducer;


