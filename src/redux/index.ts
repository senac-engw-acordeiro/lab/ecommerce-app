import { combineReducers, createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { composeWithDevTools } from 'redux-devtools-extension';
import { persistStore } from 'redux-persist'
import createReduxPromiseListener from 'redux-promise-listener';
import product from 'src/redux/reducers/product'
import rootSaga from 'src/sagas';
import { IProduct } from 'src/models/product';

export interface IStore {
  product: {
    list: IProduct[]
  }
}

const initialState = {
  product: {
    list: []
  }
};

const rootReducer = combineReducers({ product });

const sagaMiddleware = createSagaMiddleware();

const reduxPromiseListener = createReduxPromiseListener();

const store = createStore<any, any, any, any>(
  rootReducer,
  initialState,
  composeWithDevTools(applyMiddleware(sagaMiddleware, reduxPromiseListener.middleware))
);

export const promiseListener = reduxPromiseListener;

persistStore(store);

sagaMiddleware.run(rootSaga);

export default store;