import React from 'react';
import { Route, Switch, BrowserRouter, Redirect } from 'react-router-dom';
import Main from 'src/pages/Main';
import Register from 'src/pages/Register';
import Product from 'src/pages/Product';
import Header from 'src/containers/Header';
import NoMatch from 'src/pages/NoMatch';

export const ROUTES = {
  default: {
    label: 'Lista de Produtos',
    path: '/'
  },
  register: {
    label: 'Registrar',
    path: '/register'
  },
  product: {
    path: '/product'
  },
  notFound: {
    path: '/not-found'
  }
}

export default class MainRoute extends React.PureComponent {
  public render() {
    return (
      <BrowserRouter>
        <Header />
        <Switch>
          <Route exact path={ROUTES.default.path} component={Main} />
          <Route path={ROUTES.register.path} component={Register} />
          <Route path={ROUTES.product.path + '/:id'} component={Product} />
          <Route exact path={ROUTES.notFound.path} component={NoMatch} />
          <Redirect to={ROUTES.notFound.path} />
        </Switch>
      </BrowserRouter>
    )
  }
}