import { AxiosResponse } from 'axios';
import { all, call, put } from 'redux-saga/effects';
import { IReducerObjectAction } from 'src/actions';
import { CREATE_PRODUCT, createProduct, EDIT_PRODUCT, editProduct, IProductCreateRequest, IProductEditRequest, IProductRemoveRequest, LIST_PRODUCT, listProduct, REMOVE_PRODUCT, removeProduct } from 'src/actions/product';
import { createTask } from 'src/sagas';
import ProductsService, { IProductCreateResponse, IProductGetAllResponse, IProductUpdateResponse } from 'src/services/products';
import { Container } from 'typescript-ioc';

const productsService = Container.get(ProductsService);

function * create(action: IReducerObjectAction<IProductCreateRequest>) {
  try {
    const response: AxiosResponse<IProductCreateResponse> = yield call([productsService, 'create'], {
      data: action.payload
    });

    yield put(createProduct.succeeded(response?.data?.data));
  } catch (error) {
    console.log(error);

    yield put(createProduct.failed({ message: 'RIP' }));
  }
}

function * list(action: IReducerObjectAction<void>) {
  try {
    const response: AxiosResponse<IProductGetAllResponse> = yield call([productsService, 'getAll']);

    yield put(listProduct.succeeded(response?.data));
  } catch (error) {
    console.log(error);

    yield put(listProduct.failed({ message: 'RIP' }));
  }
}

function * edit({ payload: { id, ...rest }}: IReducerObjectAction<IProductEditRequest>) {
  try {
    const response: AxiosResponse<IProductUpdateResponse> = yield call([productsService, 'update'], id, {
      data: rest
    });

    yield put(editProduct.succeeded(response?.data?.data));
  } catch (error) {
    console.log(error);

    yield put(editProduct.failed({ message: 'RIP' }));
  }
}

function * remove(action: IReducerObjectAction<IProductRemoveRequest>) {
  try {
    const response: AxiosResponse<void | any> = yield call([productsService, 'delete'], {
      data: action.payload
    });

    if (response.data) {
      throw Error(response.data.error?.message);
    }

    yield put(removeProduct.succeeded(action.payload));
  } catch (error) {
    console.log(error);

    yield put(removeProduct.failed({ message: 'RIP' }));
  }
}

export default function * productSaga() {
  yield all([
    createTask<IReducerObjectAction<IProductCreateRequest>>(CREATE_PRODUCT, create),
    createTask<IReducerObjectAction<IProductEditRequest>>(EDIT_PRODUCT, edit),
    createTask<IReducerObjectAction<IProductRemoveRequest>>(REMOVE_PRODUCT, remove),
    createTask<IReducerObjectAction<void>>(LIST_PRODUCT, list),
  ])
}