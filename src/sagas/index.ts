import { all, cancel, take, takeEvery, takeLatest } from 'redux-saga/effects';
import { Stage } from 'src/actions';

import productSaga from './product';

export function * createTask<T>(action: { [key in Stage]: string }, call: any) {
  while (true) {
    const task = yield takeLatest(action.REQUEST, call)

    yield take(action.CANCELLED)
    yield cancel(task)
  }
}

export function * createAsyncTask<T>(action: { [key in Stage]: string }, call: any) {
  while (true) {
    const task = yield takeEvery(action.REQUEST, call)

    yield take(action.CANCELLED)
    yield cancel(task)
  }
}

export default function * rootSaga() {
  yield all([
    productSaga()
  ])
}