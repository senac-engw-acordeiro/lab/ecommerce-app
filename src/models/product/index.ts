export interface IProduct {
  id: number;
  sku: string;
  name: string;
  description: string;
  price: number;
  quantity: number;
  imageSrc?: any;
}

export class Product implements IProduct {
  public id: number;
  public sku: string;
  public name: string;
  public description: string;
  public price: number;
  public quantity: number;
  public imageSrc?: string;

  constructor();
  constructor(data: IProduct);
  constructor(data: Partial<IProduct>);
  constructor(data?: any) {
    this.id = data?.id;
    this.sku = data?.sku;
    this.name = data?.name;
    this.description = data?.description;
    this.price = data?.price;
    this.quantity = data?.quantity;
    this.imageSrc = data?.imageSrc || undefined;
  }
}
