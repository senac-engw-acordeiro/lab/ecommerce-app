import React from 'react';
import { CREATE_PRODUCT } from 'src/actions/product';
import ProductRegisterForm from 'src/components/ProductRegisterForm';
import Page from 'src/containers/Page';

export default () => {
  return (
    <Page>
      <ProductRegisterForm action={CREATE_PRODUCT} />
    </Page>
  )
}