import React from 'react';
import { useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import { EDIT_PRODUCT } from 'src/actions/product';
import ProductRegisterForm from 'src/components/ProductRegisterForm';
import Page from 'src/containers/Page';
import { IStore } from 'src/redux';

export default () => {
  const { id } = useParams<{ id: string }>();

  const productData = useSelector((state: IStore) => state?.product?.list.find((item) => item.id === parseInt(id)));

  return (
    <Page>
      <ProductRegisterForm
        edit
        action={EDIT_PRODUCT}
        data={productData}
      />
    </Page>
  )
}
