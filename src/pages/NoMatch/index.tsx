import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import ErrorIcon from '@material-ui/icons/Error';
import React from 'react'
import Page from 'src/containers/Page'

export default () => {
  return (
    <Page>
      <Grid container style={{ minHeight: 'inherit' }}>
        <Grid container style={{ paddingTop: 54 }}>
          <Grid
            container
            direction='column'
            justify='center'
            alignItems='center'
          >
            <Grid item>
              <ErrorIcon
                color='primary'
                style={{ fontSize: 100 }}
              />
              <Typography
                color='primary'
                align='center'
                variant='h4'
                style={{ cursor: 'default' }}
              >
                404
              </Typography>
            </Grid>
            <Grid item>
              <Typography variant='h4' style={{ cursor: 'default' }}>
                Página Não Encontrada
              </Typography>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </Page>
  )
}
