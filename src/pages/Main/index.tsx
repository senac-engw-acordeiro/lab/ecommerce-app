import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { listProduct, removeProduct } from 'src/actions/product';
import ProductList from 'src/components/ProductList';
import Page from 'src/containers/Page';
import { ROUTES } from 'src/routes';

export default () => {
  const history = useHistory();
  const dispatch = useDispatch();

  const handleEdit = (id: number) => history.push(ROUTES.product.path + `/${id}`);

  const handleRemove = (id: number) => dispatch(removeProduct.request({ id }));

  const handleAdd = () => history.push(ROUTES.register.path);

  useEffect(() => {
    dispatch(listProduct.request());
  });

  return (
    <Page>
      <ProductList
        onEdit={handleEdit}
        onRemove={handleRemove}
        onAdd={handleAdd}
      />
    </Page>
  )
}