import Container from '@material-ui/core/Container';
import Fade from '@material-ui/core/Fade';
import Grid from '@material-ui/core/Grid';
import React from 'react';

export default ({ children }: { children: JSX.Element }) => {
  return (
    <Grid>
      <Fade appear in unmountOnExit>
        <Container>
          {children}
        </Container>
      </Fade>
    </Grid>
  )
}